=== Cron Test Email Send Form ===
Contributors: NetTrackers
Tags: email, smtp, phpmailer, sendmail
Requires at least: 4.9
Tested up to: 5.0.1
Stable tag: 12
Requires PHP: 5.6

"Cron Test Email Send Form" plugin for WordPress.

== Description ==

"Cron Test Email Send Form" plugin for WordPress.

= Features =           

* Simple "Test Email Send Form".
* On dailybasis receive email after setting cron in cpanel
* Log is updated and written to
* New version email will be disabled

= Requirements =

* PHP 5.6 or higher.
* WordPress 4.9 or higher.
* Cpanel to set cron

It may work with older versions of PHP and WordPress, but we don't support anything older than the versions mentioned above.

= Usage =

Just install in your WordPress like any other plugin, activate it and fill the simple form for testing email.


== Installation ==

* This is custom pluging. Extract the initail zip file and just drop the contents in the <code>wp-content/plugins/</code> directory of your WordPress installation (or install it directly from your dashboard) and then activate it from Plugins page.


                                                                                                                        

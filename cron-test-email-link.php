<?php
if ( !defined('ABSPATH') ) {
/** Set up WordPress environment and CURL URLs */
require_once( dirname( __FILE__ ) . '/../../../wp-load.php' );
}

$logPath = __DIR__. "/timelog.txt";
$mode = 'w';

$current_time = time();
$old_currentstamp = file_get_contents($logPath);

$time_dif = DateDiffInterval($current_time, $old_currentstamp, 'D');
//echo $current_time.' - '.$old_currentstamp.' = '.$time_dif;

if($time_dif > return_cron_set_time())
{
    myplugin_send_email();
}

function return_cron_set_time() {
    global $wpdb;
    $row = $wpdb->get_row( $wpdb->prepare( "SELECT option_value FROM $wpdb->options WHERE option_name = %s LIMIT 1", 'wporg_options' ) );

    if ( is_object( $row ) ) {
        $value = $row->option_value;
    }

    $value_array = maybe_unserialize( $value );
    $email_send_to_cron_when = $value_array['email_send_to_cron_when_radio'];

    if($email_send_to_cron_when=='daily')
    {
        return "1";
    }

    if($email_send_to_cron_when=='every 3 days')
    {
        return "3";
    }

    if($email_send_to_cron_when=='weekly')
    {
        return "7";
    }

    return "1";

}


function myplugin_send_email() {

    global $wpdb, $logPath, $mode;

    $row = $wpdb->get_row( $wpdb->prepare( "SELECT option_value FROM $wpdb->options WHERE option_name = %s LIMIT 1", 'wporg_options' ) );

    if ( is_object( $row ) ) {
        $value = $row->option_value;
    }

    $value_array = maybe_unserialize( $value );

    $send_to = $value_array['email_send_to_cron_text'];
    $email_send_to_subject = $value_array['email_send_to_subject_text'];
    $email_send_to_content = $value_array['email_send_to_content_textarea'];

    $domain_name = $_SERVER['SERVER_NAME'];
    $date_fomate = date('Y/m/d');

    $email_send_to_subject = str_replace('{domain name}', $domain_name, $email_send_to_subject);
    $email_send_to_subject = str_replace('{yyyy/mm/dd}', $date_fomate, $email_send_to_subject);

    //wp_mail($send_to, $email_send_to_subject, $email_send_to_content); // send email
    // do something
    
    ### generating log start
    
    $email_send=0;
    if(wp_mail($send_to, $email_send_to_subject, $email_send_to_content))
    {
        $email_send=1;
    }
    else
    {
        $send_to_email = 'jason@w3developing.com';
        $email_send_to_subject = 'Email sending not working from '.$domain_name.' on '.$date_fomate;
        $email_send_to_content = 'Email sending is not working from '.$domain_name.' on '.$date_fomate;

        wp_mail($send_to_email, $email_send_to_subject, $email_send_to_content);
        $email_send=0;
    }

    // set post fields
    $post = [
        'date' => date('Y/m/d'),
        'domain' => $_SERVER['HTTP_HOST'],
        'email_send'   => $email_send,
    ];

    $ch = curl_init('https://w3hostingsolutions.com/apps/check_all.php');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

    // execute!
    $response = curl_exec($ch);

    // close the connection, release resources used
    curl_close($ch);

    // do anything you want with your response
    //var_dump($response);
    
    ### generating log end

    $logfile = fopen($logPath, $mode) or die("Unable to open file!");
    $current_time = time();
    fwrite($logfile, $current_time);
    fclose($logfile);
}


function DateDiffInterval($start_time, $end_time, $sUnit='H') {
//subtract $start_time-$end_time and return the difference in $sUnit (Days,Hours,Minutes,Seconds)
    $nInterval = $start_time - $end_time;
    if ($sUnit=='D') { // days
        $nInterval = $nInterval/60/60/24;
    } else if ($sUnit=='H') { // hours
        $nInterval = $nInterval/60/60;
    } else if ($sUnit=='M') { // minutes
        $nInterval = $nInterval/60;
    } else if ($sUnit=='S') { // seconds
    }
    return $nInterval;
} //DateDiffInterval
?>
<?php
/*
Plugin Name: SARS Cron Email System Test
Description: This plugin activates the SARS Plugin email test on a cron basis; the goal is to validate website emails are being sent.
Version: 13
Author: w3developing, LLC
*/

if (file_exists(dirname( __FILE__ ) . '/plugin-update-checker/plugin-update-checker.php')) {

    require_once(dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php');
    $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://bitbucket.org/w3developing/cron-test-email-form/',
        __FILE__,
        'cron-test-email-form'
    );
    $myUpdateChecker->setBranch('wpplugintest');
}

function wporg_settings_init() {
    // register a new setting for "wporg"
    register_setting( 'wporg', 'wporg_options' );

    // register a new section in the "wporg"
    add_settings_section(
        'wporg_section_developers',
        __( '', 'wporg' ),
        'wporg_section_developers_cb',
        'wporg'
    );

    // register a new field in the "wporg_section_developers" section, inside the "wporg"
    add_settings_field(
        'wporg_field_pill', // as of WP 4.6 this value is used only internally
        // use $args' label_for to populate the id inside the callback
        __( 'Email contents:', 'wporg' ),
        'wporg_field_pill_cb',
        'wporg',
        'wporg_section_developers',
        [
            'email_send_to_cron' => 'email_send_to_cron_text',
            'email_send_to_subject' => 'email_send_to_subject_text',
            'email_send_to_content' => 'email_send_to_content_textarea',
            'email_send_to_cron_when' => 'email_send_to_cron_when_radio',
            'class' => 'wporg_row',
            'wporg_custom_data' => 'custom',
        ]
    );
}

/**
 * register our wporg_settings_init to the admin_init action hook
 */
add_action( 'admin_init', 'wporg_settings_init' ); // commented by ppp

/**
 * custom option and settings:
 * callback functions
 */

// developers section cb

// section callbacks can accept an $args parameter, which is an array.
// $args have the following keys defined: title, id, callback.
// the values are defined at the add_settings_section() function.
function wporg_section_developers_cb( $args ) {
    ?>
    <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( '', 'wporg' ); ?></p>
    <?php

    $logPath = __DIR__. "/timelog.txt";
    $mode = 'w';

    if (!file_exists($logPath)) {
        $logfile = fopen($logPath, $mode) or die("Unable to open file!");
        $current_time = time();
        fwrite($logfile, $current_time);
        fclose($logfile);
    }

}

// pill field cb
// field callbacks can accept an $args parameter, which is an array.
// $args is defined at the add_settings_field() function.
// wordpress has magic interaction with the following keys: label_for, class.
// the "label_for" key value is used for the "for" attribute of the <label>.
// the "class" key value is used for the "class" attribute of the <tr> containing the field.
// you can add custom key value pairs to be used inside your callbacks.
function wporg_field_pill_cb( $args ) {
    // get the value of the setting we've registered with register_setting()
    $options = get_option( 'wporg_options' );

    // output the field
    ?>
    <style type="text/css">
        input {width: 80%; margin-bottom: 15px;}
        .submit{ width: 300px; float:right; margin-right: 5%;}
        .cron_radio_main{ padding: 5px 0;}
        .cron_radio_block{ width: 100%; padding: 15px 0;}
        .cron_radio_text{ float: left; clear: both; width: 100px;}
        .cron_radio_button{ float: left;}



    </style>
    <br />
    <strong><?php echo __( 'Send to:', 'wporg' );?></strong><br />
    <input type="text" id="<?php echo esc_attr( $args['email_send_to_cron'] ); ?>" name="wporg_options[<?php echo esc_attr( $args['email_send_to_cron'] ); ?>]" value="<?php echo $options[$args['email_send_to_cron']]; ?>" />
    <br />
    <strong><?php echo __( 'Subject:', 'wporg' );?></strong> <br />
    <input type="text" id="<?php echo esc_attr( $args['email_send_to_subject'] ); ?>" name="wporg_options[<?php echo esc_attr( $args['email_send_to_subject'] ); ?>]" value="<?php echo $options[$args['email_send_to_subject']]; ?>" />

    <div style="font-size: 14px; color: #ff0000; padding-bottom: 10px;">
        <?php echo __( '{domain name} => in the Subject put {domain name} to get doamin name.', 'wporg' );?>
        <br />
        <?php echo __( '{yyyy/mm/dd} => in the Subject put {yyyy/mm/dd} to get current date.', 'wporg' );?>
    </div>
    <strong><?php echo __( 'Content:', 'wporg' );?></strong>
    <br />
    <textarea id="<?php echo esc_attr( $args['email_send_to_content'] ); ?>" name="wporg_options[<?php echo esc_attr( $args['email_send_to_content'] ); ?>]" cols="110" rows="10"><?php echo $options[$args['email_send_to_content']]; ?></textarea>


    <br />
    <div style="padding-top: 10px;"><strong><?php echo __( 'Cron Execution:', 'wporg' );?></strong></div>

    <div class="cron_radio_main">
        <div class="cron_radio_block">
            <div class="cron_radio_text"><?php echo __( 'Daily:', 'wporg' );?></div>
            <div class="cron_radio_button"><input type="radio" id="<?php echo esc_attr( $args['email_send_to_cron_when'] ); ?>" name="wporg_options[<?php echo esc_attr( $args['email_send_to_cron_when'] ); ?>]" <?php if($options[$args['email_send_to_cron_when']]=='daily') { ?>checked="checked"<?php } ?> value="daily" /></div>
        </div>
        <div class="cron_radio_block">
            <div class="cron_radio_text"><?php echo __( 'Every 3 days:', 'wporg' );?></div>
            <div class="cron_radio_button"><input type="radio" id="<?php echo esc_attr( $args['email_send_to_cron_when'] ); ?>" name="wporg_options[<?php echo esc_attr( $args['email_send_to_cron_when'] ); ?>]" <?php if($options[$args['email_send_to_cron_when']]=='every 3 days') { ?>checked="checked"<?php } ?> value="every 3 days" /></div>
        </div>
        <div class="cron_radio_block">
            <div class="cron_radio_text"><?php echo __( 'Weekly:', 'wporg' );?></div>
            <div class="cron_radio_button"><input type="radio" id="<?php echo esc_attr( $args['email_send_to_cron_when'] ); ?>" name="wporg_options[<?php echo esc_attr( $args['email_send_to_cron_when'] ); ?>]" <?php if($options[$args['email_send_to_cron_when']]=='weekly') { ?>checked="checked"<?php } ?> value="weekly" /></div>
        </div>
    </div>

    <p class="description">
        <?php esc_html_e( '', 'wporg' ); ?>
    </p>
    <p class="description">
        <?php esc_html_e( '', 'wporg' ); ?>
    </p>
    <?php
}

/**
 * top level menu
 */
function wporg_options_page() {
    // add top level menu page
    add_menu_page(
        'Cron Test Email Send Form',
        'Cron Test Email Send Form',
        'manage_options',
        'wporg',
        'wporg_options_page_html'
    );
}

/**
 * register our wporg_options_page to the admin_menu action hook
 */
add_action( 'admin_menu', 'wporg_options_page' );

/**
 * top level menu:
 * callback functions
 */
function wporg_options_page_html() {
    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }

    // add error/update messages
    // check if the user have submitted the settings
    // wordpress will add the "settings-updated" $_GET parameter to the url
    if ( isset( $_GET['settings-updated'] ) ) {
        // add settings saved message with the class of "updated"
        add_settings_error( 'wporg_messages', 'wporg_message', __( 'Settings Saved', 'wporg' ), 'updated' );
    }

    // show error/update messages
    settings_errors( 'wporg_messages' );
    ?>
    <?php

    $current_plugin_url = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
    $cron_link = 'Example(every minute) :'.'&nbsp;&nbsp;'.' * * * * * curl -L '.$current_plugin_url.'cron-test-email-link.php';
    ?>

    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
        <form action="options.php" method="post">
            <?php
            // output security fields for the registered setting "wporg"
            settings_fields( 'wporg' );
            // output setting sections and their fields
            // (sections are registered for "wporg", each field is registered to a specific section)
            do_settings_sections( 'wporg' );
            ?>
            <div style="font-size: 16px; color: #000000; padding: 20px 0 0 200px;"><?php echo __( 'After saving, please set this below url in cpanel cron:', 'wporg' );?></div>
            <div style="font-size: 14px; color: #ff0000; padding: 5px 0 0 200px;"><?php echo $cron_link;?></div>

            <?php
            // output save settings button
            submit_button( 'Save Settings' );
            ?>
        </form>
    </div>

    <?php
}
?>